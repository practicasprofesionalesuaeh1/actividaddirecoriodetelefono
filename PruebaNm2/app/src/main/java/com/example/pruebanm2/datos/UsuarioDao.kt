package com.example.pruebanm2.datos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.example.pruebanm2.tablaBD.User
@Dao
interface UsuarioDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun addUser(user: User)

    @Delete
    suspend fun deleteUser(user: User)

    @Update
    suspend fun  updateUser(user: User)

    @Query("SELECT * FROM user_tabla ORDER BY id ASC")
    fun readAllData(): LiveData<List<User>>

}