package com.example.pruebanm2.datos.repositorio

import androidx.lifecycle.LiveData
import com.example.pruebanm2.tablaBD.User
import com.example.pruebanm2.datos.UsuarioDao

class UsuarioRepository(private val usuarioDao: UsuarioDao) {
    val readAllData: LiveData<List<User>> = usuarioDao.readAllData()

    suspend fun addUser(user: User){
        usuarioDao.addUser(user)
    }

    suspend fun  updateUser(user: User){
        usuarioDao.updateUser(user)
    }

    suspend fun  deleteUser(user: User){
        usuarioDao.deleteUser(user)
    }


}