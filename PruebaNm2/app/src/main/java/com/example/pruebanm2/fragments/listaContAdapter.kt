package com.example.pruebanm2.fragments

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.example.pruebanm2.R
import com.example.pruebanm2.tablaBD.User
import kotlinx.android.synthetic.main.ver_contactos.view.*

class listaContAdapter: RecyclerView.Adapter<listaContAdapter.MyViewHolder>(){

    private var userList = emptyList<User>()

    class MyViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) { }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.ver_contactos, parent, false))
    }

    override fun getItemCount(): Int {
        return userList.size
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val currentItem = userList[position]
        holder.itemView.textView2.text = currentItem.id.toString()
        holder.itemView.textView3.text = currentItem.nombre
        holder.itemView.textView4.text = currentItem.telefono

        holder.itemView.rowLayout.setOnClickListener {
            val action = ContactosFragmentDirections.actionContactosFragmentToActualizarFragment(currentItem)
            holder.itemView.findNavController().navigate(action)
        }

    }

    fun setData(user: List<User>){
        this.userList = user
        notifyDataSetChanged()
    }

}