package com.example.pruebanm2.fragments

import android.os.Bundle
import android.text.TextUtils
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import com.example.pruebanm2.R
import com.example.pruebanm2.tablaBD.User
import com.example.pruebanm2.viewmodel.UsuarioViewModel
import kotlinx.android.synthetic.main.fragment_agregar.*
import kotlinx.android.synthetic.main.fragment_agregar.view.*


class AgregarFragment : Fragment() {
    private lateinit var mUserViewMode: UsuarioViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_agregar, container, false)

        mUserViewMode = ViewModelProvider(this).get(UsuarioViewModel::class.java)

        view.button_agregar.setOnClickListener {
            insertDataToDatabase()
        }
        return  view
    }

    private fun insertDataToDatabase() {
        val nombre = agregarNombre_et.text.toString()
        val telefono = agregarTelefono_et.text.toString()
        if(inputCheck(nombre,telefono)){
            val user = User(0, nombre, telefono)

            mUserViewMode.addUser(user)
            Toast.makeText(requireContext(), "agregado correctamente", Toast.LENGTH_LONG).show()

          findNavController().navigate(R.id.action_agregarFragment_to_contactosFragment)
        }else{
            Toast.makeText(requireContext(), "llena los campos para continiar", Toast.LENGTH_LONG).show()
        }
    }

    private  fun inputCheck(nombre: String, telefono: String): Boolean{
        return !(TextUtils.isEmpty(nombre)&& TextUtils.isEmpty(telefono))
    }


}