package com.example.pruebanm2.tablaBD

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity(tableName = "user_tabla")
class User (
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val nombre: String,
    val telefono: String

): Parcelable