package com.example.pruebanm2.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ListAdapter
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.pruebanm2.R
import com.example.pruebanm2.viewmodel.UsuarioViewModel
import kotlinx.android.synthetic.main.fragment_contactos.view.*

class ContactosFragment : Fragment() {

    private lateinit var  mUsuarioViewModel: UsuarioViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_contactos, container, false)

        //recyclerview
        val adapter = listaContAdapter()
        val recyclerView = view.recyclerView
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        // UserViewModel
        mUsuarioViewModel = ViewModelProvider(this).get(UsuarioViewModel::class.java)
        mUsuarioViewModel.readAllData.observe(viewLifecycleOwner, Observer{ user ->
            adapter.setData(user)
        })



        view.floatingActionButton.setOnClickListener {
            findNavController().navigate(R.id.action_contactosFragment_to_agregarFragment)
        }
        return view
    }


}