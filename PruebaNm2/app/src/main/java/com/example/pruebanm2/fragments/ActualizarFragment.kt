package com.example.pruebanm2.fragments

import android.app.AlertDialog
import android.os.Bundle
import android.text.TextUtils
import android.view.*
import androidx.fragment.app.Fragment
import android.widget.Toast
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.pruebanm2.R
import com.example.pruebanm2.tablaBD.User
import com.example.pruebanm2.viewmodel.UsuarioViewModel
import kotlinx.android.synthetic.main.fragment_actualizar.*
import kotlinx.android.synthetic.main.fragment_actualizar.view.*


class ActualizarFragment : Fragment() {

    private val args by navArgs<ActualizarFragmentArgs>()

    private  lateinit var mUserViewModel: UsuarioViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_actualizar, container, false)

        mUserViewModel = ViewModelProvider(this).get(UsuarioViewModel::class.java)

        view.actualizarNombre_et.setText(args.usuarioActual.nombre)
        view.actualizarTelefono_et.setText(args.usuarioActual.telefono)
        
        view.button_actualizar.setOnClickListener {
            updateItem()
        }

        setHasOptionsMenu(true)
        return view
    }
    private fun updateItem(){
        val nombre = actualizarNombre_et.text.toString()
        val telefono = actualizarTelefono_et.text.toString()

        if(inputCheck(nombre, telefono)){
            // crear el objeto usuario
            val updatedUser = User(args.usuarioActual.id, nombre, telefono)
            //actualizar usuario actual
            mUserViewModel.updateUser(updatedUser)
            Toast.makeText(requireContext(), "Updated Successfully!", Toast.LENGTH_SHORT).show()
            //navegador
            findNavController().navigate(R.id.action_actualizarFragment_to_contactosFragment)
        }else{
            Toast.makeText(requireContext(), "error!", Toast.LENGTH_SHORT).show()
        }

    }
    private  fun inputCheck(nombre: String, telefono: String): Boolean{
        return !(TextUtils.isEmpty(nombre)&& TextUtils.isEmpty(telefono))
    }
    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.borrar_user, menu)
    }
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId==R.id.menu_delete){
            deleteUser()
        }
        return super.onOptionsItemSelected(item)
    }

    private fun deleteUser() {
    val builder = AlertDialog.Builder(requireContext())
        builder.setPositiveButton("Yes"){ _, _ ->
            mUserViewModel.deleteUser(args.usuarioActual)
            Toast.makeText(requireContext(),"Eliminado correctamente: ${args.usuarioActual.nombre}",Toast.LENGTH_SHORT).show()
            findNavController().navigate(R.id.action_actualizarFragment_to_contactosFragment)

        }
        builder.setNegativeButton("No"){ _, _ ->}
        builder.setTitle("Eliminar ${args.usuarioActual.nombre}?")
        builder.setMessage("Estas seguro ed elimiar elcontacto ${args.usuarioActual.nombre}")
        builder.create().show()
    }


}